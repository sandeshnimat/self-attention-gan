## Generative Adversarial Networks
------------------------------------------------------------------------------------
<!-- will add information as development progresses -->
This is Tensorflow Keras based implementation of :

1. Deep Convolutional Generative Adversarial Network
   
    * This implementation is based on architectural constraints suggested by Alec Radford, Luke Metz, Soumith Chintala in [`Unsupervised Representation Learning with Deep Convolutional Generative Adversarial Networks`](https://arxiv.org/abs/1511.06434) for stable training.
     
2. Conditional Self Attention Generative Adversarial Network
    * Based on [`Self-Attention Generative Adversarial Networks`](https://arxiv.org/abs/1805.08318) by Han Zhang, Ian Goodfellow, Dimitris Metaxas, Augustus Odena, this implementation tries to improve over DCGAN's performance using Self Attention.

If unable to open IPython notebook, copy and paste the notebook's link [here](https://nbviewer.jupyter.org/).